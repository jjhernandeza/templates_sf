# templates_sf

Formatos, Platillas y autoayuda de proyectos

## Empezando


Platillas, Formulario, Funciones, Expresiones, Métodos, Modelos, Clases y Servicios ( Propios y de tipo HTTP ) para la gestión y desarrollo de Proyectos

Estas plantillas tienen como finalidad crear la autoayuda, para el desarrollo de aplicaciones creadas con Angular CLI, 


## Pasos de instalación

Los archivos en cada directorio del ropositorio son de la extención de un proyecto angular del directorio src, por lo que para crear un correcto funcionamiento de las plantillas es neseario
seguir lo siguiente:

1. Crear un proyecto nuevo angular cli
2. Las plantillas hacen uso de Bootstrap ( ultima versión ), por lo que requiere de instalación por CDN o por NPM
3. Requiere de Bootstrap, Jquery, Poppers.js y Fontawesome:
	- [ ] [3.1. Instalación por CDN o por NPM]
	- [ ] [3.2. Antes de generar las instalaciones correspondiente; revisar el archivo de configuración (angular.json), de cada plantilla]
4. Una véz creado el proyecto copiar el directorio de cada plantilla en el proyecto en el directorio "src"
5. Para las plantillas que incluyan el archivo angular.json ( copiar y remplanzar por el que sea creado en su proyecto ), contiene las configuraciones en la que se implemento cada espacio de 
   trabajo



## Importante Leer!!

No crear un instancia desde el proyecto de o desde el directorio del repositorio ya que puede crear modificaciones al 	
- [ ] proyecto real. 

Por lo que se sugiere solo copiar el contenido de cada dictorio del repositorio de la plantilla que desea.

