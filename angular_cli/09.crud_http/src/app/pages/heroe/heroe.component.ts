import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";

//Servicios
import { HeroesService } from "../../services/heroes/heroes.service";
import {ActivatedRoute, Router} from "@angular/router";

import Swal from 'sweetalert2';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  parametro : any =
  {
      ID_HEROE     : null
      ,NOM_HEROE   : null
      ,PODER_HEROE : null
      ,VIVO        : true
      ,LBL_VIVO    : 'Vivo'
  };

  validacion : any =
  {
      MSG : null
  }


  constructor( private _heroesService :HeroesService, private variableRoute : ActivatedRoute )
  {
      this.parametro.VIVO = true;
  }

  ngOnInit(): void
  {
      //this.variableRoute.snapshot.paramMap.get('id') forma alternativa para obtener datos del url.

      //Se asignar valor en caso de que el registro sea edición o captura
      this.variableRoute.params.subscribe( param =>
      {
          this.parametro.ID_HEROE = (param['id'] === 'nuevo') ? null : (param['id'] !== null || param['id'] !== '' || param['id']) ? param['id'] : null;

          if( this.parametro.ID_HEROE !== null )
          {
              this._heroesService.obtener_heroe( this.parametro ).subscribe( ( reps : any ) =>
              {
                  this.parametro.NOM_HEROE   = reps.NOM_HEROE;
                  this.parametro.PODER_HEROE = reps.PODER_HEROE;
                  this.parametro.VIVO        = reps.VIVO;
                  this.parametro.LBL_VIVO    = reps.LBL_VIVO;
              });
          }
          else
          {
              this.parametro  =
              {
                  ID_HEROE     : null
                  ,NOM_HEROE   : null
                  ,PODER_HEROE : null
                  ,VIVO        : true
                  ,LBL_VIVO    : 'Vivo'
              };
          }

      });
  }

  guardar( frmHeroe : NgForm )
  {
      if( frmHeroe.form.status === "INVALID" )
          return Object.values( frmHeroe.form.controls ).forEach( control =>  control.markAllAsTouched() );

      this._heroesService.validarRegistro( this.parametro )
          .subscribe( reps =>
          {
              this.validacion.MSG = reps;

              if( this.parametro.ID_HEROE === null )
                this.limpiarFormulario( frmHeroe );

              Swal.fire({
                icon: 'success',
                title: ( this.parametro.ID_HEROE !== null ) ?'Datos actualizados correctamente':`Hereo registrado con número de registro: ${ this.validacion.MSG }`,
                showConfirmButton: true,
                timer: 1500
              })
          });
  }

  validarHeroe()
  {
      this.parametro.VIVO     = !this.parametro.VIVO;
      this.parametro.LBL_VIVO = ( this.parametro.LBL_VIVO === 'Vivo' ) ? 'Muerto' : 'Vivo';
  }

  limpiarFormulario( frmHeroe : NgForm )
  {
    this.parametro =
      {
        ID_HEROE     : null
        ,NOM_HEROE   : null
        ,PODER_HEROE : null
        ,VIVO        : true
        ,LBL_VIVO    : 'Vivo'
      };

      Object.values( frmHeroe.form.controls ).forEach( control =>  control.markAsUntouched() );
  }


}
