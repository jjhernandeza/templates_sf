import { Component, OnInit } from '@angular/core';
import { HeroesService } from "../../services/heroes/heroes.service";
import {ActivatedRoute, Router} from "@angular/router";

import Swal from 'sweetalert2';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit
{

    dataHereos : any = [];
    loading    : boolean = false;
    constructor( private _hereosServices : HeroesService, private _routerLink : Router )
    { }

    ngOnInit(): void
    {
        this.obtener_registros();
    }

    obtener_registros()
    {
        this.loading = true;
        this._hereosServices.obtener_registros()
            .subscribe( ( data : any ) =>
            {
                this.dataHereos = data;
                this.loading = false;
            });
    }

    actualiarRegistro( idx : number )
    {
        this._routerLink.navigate( [ '/heroe', this.dataHereos[ idx ].ID_HEROE ] );
    }

    eliminarHeroe( index : number )
    {
        Swal.fire({
            title: `Estas seguro que deseas borrar el hereo: ${ this.dataHereos[ index ].NOM_HEROE }?`,
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then(( rps ) =>
        {
             if( rps.isConfirmed )
             {
                  this._hereosServices.borrarHereo( this.dataHereos[ index ].ID_HEROE )
                      .subscribe
                      (
                          ( respuesta : any ) =>
                          {
                              this.dataHereos.splice( index, 1 );

                              Swal.fire(
                                  'Eliminado!',
                                  'Registro eliminado.',
                                  'success'
                                );
                          }
                      );

             }
        });
    }
}
