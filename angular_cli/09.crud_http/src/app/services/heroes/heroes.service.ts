import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {delay, map} from "rxjs/operators";

//el operador map permite transformar la data de respuesta de petición proveniente de un apirest

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private url_services = "https://login-app-a6a87-default-rtdb.firebaseio.com";

  constructor( private _httpClient: HttpClient) { }

  //Servicio para obtener Hereos Registrados
  obtener_registros()
  {
      //Petición de Tipo post. ( url_petición, data( body, objeto ) )
      return this._httpClient.get( ` ${ this.url_services }/heroes.json` )
          .pipe(
              map( ( reps : any ) =>
              {
                  return this.crearArreglo_1( reps );
                  //return this.crearArreglo_2( reps );
              }),
            delay( 1500 )
          );
  }

  //Forma para leer y crear un arreglo desde un objeto tipo any
  private crearArreglo_1( dataHereos : any )
  {
      let result = [],
        registros: any = [];

      for( let i in dataHereos )
          result.push( [ i, dataHereos [i] ] );

      result.forEach( ( value ) =>
      {
          value.forEach( data =>
          {
              if( data.NOM_HEROE )
                  registros.push( { ID_HEROE : value[0] , NOM_HEROE : data.NOM_HEROE, PODER_HEROE: data.PODER_HEROE, ESTATUS : data.VIVO, LBL_VIVO : data.LBL_VIVO } );
          });
      });

      return registros;
  }

  //Creando un arreglo desde un objeto
  private crearArreglo_2( dataHereos : object )
  {
      let registros : any = [];
      let valores   : any = [];

      Object.keys( dataHereos ).forEach( ( key) =>
      {
          Object.values( dataHereos ).forEach( value =>
          {
              if(  registros.ID_HEROE !== key )
              {
                  valores =
                  {
                    ID_HEROE     : key
                    ,NOM_HEROE   : value.NOM_HEROE
                    ,PODER_HEROE : value.PODER_HEROE
                    ,ESTATUS     : value.VIVO
                    ,LBL_VIVO    : value.LBL_VIVO
                  };
              }
          });

          registros.push( valores )
      });

      return registros;
  }

  //Valida el tipo de Servicio para Edición o Registro
  validarRegistro ( dataHeroe : any )
  {
      if( dataHeroe.ID_HEROE === null || dataHeroe.ID_HEROE === '')
          return this.crearHeroe( dataHeroe );
      else
          return this.actualizarHeroe( dataHeroe );
  }

  crearHeroe( dataHero : any )
  {
      //Petición de Tipo post. ( url_petición, data( body, objeto ) )
      return this._httpClient.post( ` ${ this.url_services }/heroes.json`, dataHero )
          .pipe(
              map( ( reps : any ) =>
              {
                  return reps.name;
              })
          );
  }

  actualizarHeroe( dataHero : any )
  {
      //Sintaxis para copiar un objeto y todas sus propiedades a un nuevo elemento o objeto
      const dataTemp = { ...dataHero };

      //Sintaxis para eliminar una propiedad de un objeto o posición de un objeto
      delete dataTemp.ID_HEROE;

      return this._httpClient.put( ` ${ this.url_services }/heroes/${ dataHero.ID_HEROE }.json`, dataHero )
          .pipe(
              map( ( resp : any ) =>
              {
                  return dataHero.ID_HEROE ;
              })
          )
  }

  obtener_heroe( dataHero : any )
  {
      return this._httpClient.get( ` ${ this.url_services }/heroes/${ dataHero.ID_HEROE }.json` )
          .pipe(
              map( ( reps : any ) =>
              {
                  return reps;
              })
          );
  }

  borrarHereo ( id :string )
  {
      return this._httpClient.delete( ` ${ this.url_services }/heroes/${ id }.json` );
  }

}
