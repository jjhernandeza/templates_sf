    import { Component } from '@angular/core';

    @Component
    ({
        selector    : 'app-body',
        templateUrl : './body.component.html'
    })

    export class bodyComponent
    {
        mostrar:boolean = true;

        frase : any =
        {
            MENSAJE  : 'Un gran poder requeire una responsabilidad'
            ,AUTOR   : 'Ben Parker'
        };

        personajes: string[] = [ 'Spider', 'Vemon', 'Dr. Octupus'];
    }
