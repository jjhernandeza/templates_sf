import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

//Componetes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { NavbarComponent } from './components/search/navbar/navbar.component';
import { MainComponent } from './components/search/main/main.component';
import { TarjetasComponent } from './components/tarjetas/tarjetas.component';

//Router
import { ROUTES } from './app.routes';

//Pipes
import { ValidarImgPipe } from './pipes/validar-img/validar-img.pipe';
import { DominioseguroPipe } from './pipes/dominioseguro/dominioseguro.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArtistaComponent,
    NavbarComponent,
    MainComponent,
    ValidarImgPipe,
    TarjetasComponent,
    DominioseguroPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    RouterModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
