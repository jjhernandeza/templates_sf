  import { Component, Input, OnInit } from '@angular/core';
  import { Router } from '@angular/router';

  @Component({
    selector: 'app-tarjetas',
    templateUrl: './tarjetas.component.html',
    styleUrls: ['./tarjetas.component.css']
  })
  export class TarjetasComponent
  {
      @Input() items_target : any[] = [];

      constructor( private _router: Router ) { }

      verArtista(  data: any )
      {
          const cve_artista = ( data.type === 'artist' ) ? data.id  : data.artists[0].id;

          this._router.navigate(['/artista/', cve_artista]).then(r => console.log( r ));
      }
  }
