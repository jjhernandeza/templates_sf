  import { Component } from '@angular/core';
  import { SpotifyService } from "../../services/spotify/spotify.service";

  //Permisos para hacer una petaicón AJAX
  @Component({
      selector   : 'app-home',
      templateUrl: './home.component.html',
      styleUrls  : ['./home.component.css']
  })

  export class HomeComponent
  {
      //paises :any[] = [];  //Ejemplo como solicitudar tipo .get();
      nuevas_canciones : any[]   = [];
      loading          : boolean = true;
      dataError        : boolean = false;
      mensaje_error    : string  = "";

      constructor( private _serviceSpotify : SpotifyService )
      {
          //1) Ejemplo como solicitudar tipo .get();
          /*this._httpAjax.get('https://restcountries.com/v3.1/lang/spa' )
              .subscribe( ( respuesta: any ) =>
              {
                  this.paises = respuesta;
                  console.log( respuesta );
              });*/

          //2) Ejemplo de solicitud simple por medio de Promesa de un servicio
          /*let lanzamientos = new Promise<object>( ( resolve, reject ) =>
          {
              //Se espefica el tipado de datos any debido a que se desconoce el tipo de formato proveniente del servicio
              this._serviceSpotify.obtenerNuevosLanzamientos().subscribe( (e : any) =>
              {
                  if(e)
                    resolve( { "error":true, "data": e.albums } );
                  else
                    reject( { "error":false, "data": null } );
              });
          });

          lanzamientos.then( ( rps: any ) => { if (rps.error) {  this.nuevas_canciones = rps.data.items;  console.log( this.nuevas_canciones );  } } );*/

          this._serviceSpotify.obtenerNuevosLanzamientos()
              .subscribe( data =>
              {
                  this.nuevas_canciones = data;
                  this.loading          = false;
                  this.dataError        = false;
              },
              ( error ) =>
              {
                  let data : any[]= [];
                  Object.entries( error.error ).map( key =>  data.push( key[1] )  );

                  this.loading       = false;
                  this.dataError     = true;
                  this.mensaje_error = data[0].message;
              }
          );
      }
  }
