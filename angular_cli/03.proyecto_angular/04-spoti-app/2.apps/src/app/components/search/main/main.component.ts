    import { Component } from '@angular/core';
    import { SpotifyService } from "../../../services/spotify/spotify.service";
    import { TarjetasComponent } from '../../tarjetas/tarjetas.component';

    @Component({
      selector: 'app-main',
      templateUrl: './main.component.html',
      styleUrls: ['./main.component.css']
    })

    export class MainComponent
    {
        artistas : any[]   = [];
        loading  : boolean = true;
        constructor( private _serviceSpotify : SpotifyService ) { };

        buscarArtista ( termino : string )
        {
            //1) Ejemplo de solicitud Sencilla de peticiones hacia un servicio
             /* let consulta = new  Promise <any>( ( resolve, reject ) =>
              {
                  this._serviceSpotify.consultar_artistas( termino ).subscribe( ( e : any ) =>
                  {
                      if(e)
                          resolve( { "error":true, "data": e.artists } );
                      else
                          reject( { "error":false, "data": null } );
                  });
              });

              consulta.then ( ( rps : any ) =>
              {
                  if (rps.error)
                    this.artistas = rps.data.items;

                  console.log( this.artistas );
              });*/


            //2)
          //console.log( this._serviceSpotify.consultar_artistas( termino ).subscribe( (data : any ) => { this.artistas = ( data.length > 0 ) ? data : []; this.loading = false; } ) );
            if ( termino.length > 0 )
                this._serviceSpotify.consultar_artistas( termino ).subscribe( (data : any ) => { this.artistas = ( data.length > 0 ) ? data : []; this.loading = false; } );
            else
                this.artistas = []; this.loading = true;


        }

    }
