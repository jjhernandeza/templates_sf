  import { Component, OnInit } from '@angular/core';
  import { ActivatedRoute } from "@angular/router";
  import {SpotifyService} from "../../services/spotify/spotify.service";

  @Component({
    selector: 'app-artista',
    templateUrl: './artista.component.html',
    styleUrls: ['./artista.component.css']
  })
  export class ArtistaComponent
  {
      artista   : any = {};
      toptracks : any = [];
      loading   : boolean = true;

      constructor( private _Activerouter: ActivatedRoute, private _spotifyService : SpotifyService )
      {
          this._Activerouter.params.subscribe(data =>
          {
              let toptracks = this.obtener_topTracks (data.id );

              toptracks.then( ( topTracks ) =>
              {
                  console.log( topTracks );
                  if ( topTracks )
                  {
                      this.loading   = false;
                      this.toptracks = topTracks;
                      this.obtenerDatos( data.id );
                  }
              });
          });
      }

      obtenerDatos ( id:string )
      {
          this._spotifyService.historiaArtista( id ).subscribe( data => this.artista = data );
      }

      obtener_topTracks ( cve_artista: string )
      {
          return new Promise<any>( ( resolve, rejects ) =>
          {
              this._spotifyService.obtener_toptracks( cve_artista )
                  .subscribe( ( data : any ) =>
                  {
                      if( data )
                          return resolve( data );
                      else
                          return rejects( false );
                  });
          });
      }
  }
