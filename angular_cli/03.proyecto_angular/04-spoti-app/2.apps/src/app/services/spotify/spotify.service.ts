  import { Injectable } from '@angular/core';
  import { HttpClient, HttpHeaders } from "@angular/common/http";
  import {  map } from 'rxjs/operators';

  //Forma automática de crear un servicio por medio un decorador
  @Injectable({
    providedIn: 'root'
  })
  export class SpotifyService
  {
      //token :string;

      constructor( private _httpAjax: HttpClient )
      {
          //Inicializar variables
          //this.token = 'BQDaHaLc1KWe3yDi4CL-qUMfsTtQTnsvLpCnDXxuQ6h0SKrySWUQnu1qgZE3pr7g2UyhnW9KkAC5ZgSfIm4';
      }

      obtenerServicio ( extencion: string )
      {
          //const headers = new HttpHeaders (  { 'Authorization'   : `Bearer ${ this.token }`, } ),
          const headers = new HttpHeaders (  { 'Authorization'   : 'Bearer BQBLNvN3mzqynOnUmJBVCBg-TOhcAu9tPEmBOIfpfWxbhOiwb556ShXhZEddvVMYeHLqffaMopt9QRWefos', } );
          const url     = `https://api.spotify.com/v1/${ extencion }`;

          return this._httpAjax.get( url, { headers } );
      }

      obtenerNuevosLanzamientos ()
      {
          /*1)Uso de Petición por servicio
           const headers = new HttpHeaders (  { 'Authorization'   : `Bearer ${ this.token }`, } );
           return this._httpAjax.get('https://api.spotify.com/v1/browse/new-releases',{ headers } );*/

          /*2) Uso de Map ( Desestrucción de objetos o elementos )
          const headers = new HttpHeaders (  { 'Authorization'   : `Bearer ${ this.token }`, } );
          return this._httpAjax.get('https://api.spotify.com/v1/browse/new-releases',{ headers } ).pipe(  map(( e : any ) => e['albums'].items )  );*/

          //3)Uso de servicios  y/o optimización de código
          return this.obtenerServicio( 'browse/new-releases?limit=20' ).pipe(  map(( e : any ) => e['albums'].items ) );

      };

      consultar_artistas( termino: string )
      {
          /*1)Uso de Petición por servicio
          const headers = new HttpHeaders (  { 'Authorization'   : `Bearer ${ this.token }`, } );
          return this._httpAjax.get(`https://api.spotify.com/v1/search?q=${ termino }&type=artist&limit=15`,{ headers } );*/

          /*2) Uso de Map ( Desestrucción de objetos o elementos )
          const headers = new HttpHeaders (  { 'Authorization'   : `Bearer ${ this.token }`, } );
          return this._httpAjax.get(`https://api.spotify.com/v1/search?q=${ termino }&type=artist&limit=15`,{ headers } ).pipe(  map(( e : any ) => e['artists'].items )  );*/

          //3)Uso de servicios  y/o optimización de código
          return this.obtenerServicio( `search?q=${ termino }&type=artist&limit=15` ).pipe(  map(( e : any ) => e['artists'].items )  );
      };

      historiaArtista ( cve_artista : string )
      {
          return this.obtenerServicio( `artists/${ cve_artista }` );
      }

      obtener_toptracks ( cve_artista: string )
      {
          return this.obtenerServicio( `artists/${ cve_artista }/top-tracks?country=us` ).pipe(  map(( e : any ) => e['tracks'] )  );
      }
  }
