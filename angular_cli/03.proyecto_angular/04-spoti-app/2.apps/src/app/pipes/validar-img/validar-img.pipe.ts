  import { Pipe, PipeTransform } from '@angular/core';

  @Pipe({
    name: 'validarImg'
  })
  export class ValidarImgPipe implements PipeTransform
  {

      transform( imagenes: any[] ): string
      {
          let url: string = "assets/img/noimage.png",
              data : any[] = [];

          if ( !imagenes )
              return url;

          Object.entries( imagenes ).map( key =>  data.push( key )  );

          if( data[1][1].length > 0 )
                return data[1][1];
          else
              return url;
      }

  }
