
  import { Pipe, PipeTransform } from '@angular/core';
  import { DomSanitizer,  SafeResourceUrl } from '@angular/platform-browser';

  @Pipe({
    name: 'dominioseguro'
  })

  export class DominioseguroPipe implements PipeTransform
  {
      constructor( private _domSanitizer : DomSanitizer ) { }

      transform( value: string ):  SafeResourceUrl
      {
          const url = 'https://open.spotify.com/embed?uri=';

          //Permite crear seguridad y pases para URL de confianzas
          return this._domSanitizer.bypassSecurityTrustResourceUrl( url + value );
      }
  }
