import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MainComponent } from './components/search/main/main.component';
import { ArtistaComponent } from './components/artista/artista.component';

export const ROUTES: Routes =
[
    { path : 'home', component: HomeComponent },
    { path : 'search', component: MainComponent },
    { path : 'artista/:id', component: ArtistaComponent },
    { path : '',  pathMatch: 'full', redirectTo: 'home' },
    { path : '**', pathMatch: 'full', redirectTo: 'home' },
];
