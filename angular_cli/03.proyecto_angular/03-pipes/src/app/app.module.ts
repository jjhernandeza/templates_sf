import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID } from '@angular/core';

import { registerLocaleData } from '@angular/common'
import localEs from '@angular/common/locales/es';
import localFr from '@angular/common/locales/fr';

registerLocaleData( localEs );
registerLocaleData( localFr );

import { AppComponent } from './app.component';
import { CapitalizadoPipe } from './pipes/capitalizado/capitalizado.pipe';
import { DomseguroPipe } from './pipes/domseguro/domseguro.pipe';
import { PasswordPipe } from './pipes/password/password.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CapitalizadoPipe,
    DomseguroPipe,
    PasswordPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {
        provide   : LOCALE_ID
        ,useValue : 'es'
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
