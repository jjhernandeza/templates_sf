import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
    nombre     :string  = "Steve Roger";
    nombre_2   :string  = "jUan JosÉ HerNÁNDeZ aNTOnIO";
    arreglo    :any     = [1,2,3,4,5,6,7,8,9,10];
    pi         :number  = Math.PI;
    porcentaje :number  = 0.321654;
    salario    :number  = 12345.6;
    fecha      :Date    = new Date();
    idioma     :string  = 'es';
    idioma_lbl :string  = 'Español';
    videoURL   :string  = 'https:www.youtube.com/mebed/-CBKW-90VxQ';
    password   :string  = 'elprimervengador#';
    activar    :boolean = true;
    lbl_btn_1  :string  = 'Ver password';

    heroe =
        {
            nombre     : 'Loga'
            ,clave     : 'Wolverine'
            ,edad      : 500
            ,direccion : { calle : "1ro de Mayo" ,casa: "marve" }
        };

    valorPromesa = new Promise<string>( ( resolve, reject) => { setTimeout( ()=>resolve('Llego la data'), 4500 ); });

    cambiar_idioma( index:number )
    {
        switch ( index)
        {
            case 1:
                this.idioma_lbl = 'Español';
                this.idioma = 'es';
                break;

            case 2:
                this.idioma_lbl = 'Inglés';
                this.idioma = 'en';
                break;

            case 3:
                this.idioma_lbl = 'Francés';
                this.idioma = 'fr';
                break;
        }
    }

    showpassword ()
    {
        this.activar   = !this.activar;
        this.lbl_btn_1 = ( this.activar ) ? 'Ver password' : 'Ocultar password';
    }
}
