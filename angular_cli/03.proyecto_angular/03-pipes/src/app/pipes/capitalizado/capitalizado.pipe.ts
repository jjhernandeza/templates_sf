import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

    //Valores del tipo string, definición de tipo de salida
    transform(value: string, tipo:boolean = true): string
    {
        /*value = value.toLowerCase();
        let nombres = value.split( ' ' ),
            valor: string = "";

        if( tipo )
            nombres = nombres.map( nombre => { return nombre[0].toUpperCase() + nombre.substr(1); } );
        else
            valor = nombres[0][0].toUpperCase() + value.substr(1);

        console.log( ( valor.length > 0 ) ? valor : nombres );

        return ( valor.length > 0 ) ? value : nombres.join(' ');*/

        value = value.toLowerCase();
        let nombres = value.split( ' ' );

        if( tipo )
            nombres = nombres.map( nombre => { return nombre[0].toUpperCase() + nombre.substr(1); } );
        else
            nombres[0] = nombres[0][0].toUpperCase() + nombres[0].substr(1);

        return nombres.join(' ');
    }
}
