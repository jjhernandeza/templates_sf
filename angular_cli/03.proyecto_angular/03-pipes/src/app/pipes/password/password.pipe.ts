import { Pipe, PipeTransform } from '@angular/core';
import {count} from "rxjs/operators";

@Pipe({
  name: 'password'
})
export class PasswordPipe implements PipeTransform
{

    transform(value: string, activo : boolean = true ): string
    {
        //Solución propuesto
        /*let data  : string[] = value.split( ''),
            campo : string   = "";

        for ( let i = 0; i <= data.length ; i++ )
        {
            if( data[i] !== undefined )
              campo +=  data[i].replace( data[i], '*');
        }

        if( activo )
           value = campo;

        return  value;*/

        return ( activo ) ? '*'.repeat( value.length ) : value; //Repite el número de caráteres según la longitud de alguna variable o parametro
    }

}
