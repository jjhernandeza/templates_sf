import { Component, OnInit } from '@angular/core';
import { DataHeroes, HeroesService } from '../../services/heroes.service';
import { Router } from '@angular/router';
// /**/import { HeroeTarjetaComponent } from '../heroe-tarjeta/heroe-tarjeta.component'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: [
  ]
})
export class HeroesComponent implements OnInit
{
    heroes:DataHeroes[] = [];
    superH:any = {};

    //_heroesService:HeroesService Asignación de un alias para el acceso del alias
    constructor( private _heroesService:HeroesService, private _routerServ:Router) {}

    ngOnInit(): void
    {
        this.heroes = this._heroesService.obtenerHeroes();//Función declarada en el Servicio e invocada por la Variable o Alias asignada al Servicio
    }

    verHeroe ( index:number )
    {
        console.warn( this.superH = this.heroes[ index ] );
        this._routerServ.navigate( [ '/heroe', index ] );
    }
}
