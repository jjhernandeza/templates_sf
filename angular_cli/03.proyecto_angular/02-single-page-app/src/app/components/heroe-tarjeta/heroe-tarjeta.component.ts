import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
})
export class HeroeTarjetaComponent implements OnInit {


  //Decorador para Indicar la comunicación entre un componente a otro ( padre-hijo/ hijo padre )
  @Input() tgHeroe  :any = {};
  @Input() indice  ?: number;
  //private _idx!: number;


  //Permite
  @Output() hereoSeleccioando : EventEmitter<number>

  constructor( private _urlCompnente:Router )
  {
      //Se selecciona el Evento de Emisión para el componente padre
      this.hereoSeleccioando = new EventEmitter();
  }

  ngOnInit(): void {
  }

  verHeroe (  )
  {
      //Salida por Decorador Input Comunicaciónn de Enlace de Componente Padre
      /*console.log( this.indice );
      this._urlCompnente.navigate( [ '/heroe', this.indice ] );*/

      //Salida por Decorador Input Comunicaciónn de Enlace de Componente Hijo -> Padre
      this.hereoSeleccioando.emit( this.indice );
  }

}