import { Component, OnInit } from '@angular/core';
import { DataHeroes, HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
  ]
})
export class HeroeComponent implements OnInit {

  //heroe:DataHeroes[] = [];
  hereo:any = {};

  constructor(
                private _dataHeroe    : HeroesService,
                private variableRoute : ActivatedRoute,
                private _dirPage      : Router
              ) { }

  ngOnInit(): void
  {

    //Observacadores
      let data:number;
      //this.variableRoute.params.subscribe( parametro => { data = parametro['id'] * 1 }  );
      this.variableRoute.params.subscribe( parametro =>
      {
          data = parametro['id'] * 1;
          this.hereo = this._dataHeroe.obtenerHeroe( data );
      });
  };

  regresar()
  {
    this._dirPage.navigate( [ '/heroes' ] );
  };

}
