import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit
{

    bandera:boolean = false;

    constructor( private _hereoService:HeroesService, private _dirNavbar:Router ) { }

    ngOnInit(): void
    {
        this.bandera = false;
    }

    buscarHeroe( termino:string )
    {
        this.bandera = false;

        let index = this._hereoService.buscarHeroes( termino );

        if( index )
            this._dirNavbar.navigate( [ '/heroe', index ] );
        else
            this.bandera = true;//alert( "No se han encontrado coicicdencias en la búsqueda ");
    }

}
