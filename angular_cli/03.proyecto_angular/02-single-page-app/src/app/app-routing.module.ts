import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { SobreComponent } from './components/sobre/sobre.component';
import { HeroeComponent } from './components/heroe/heroe.component';

//Asignar Módulos, Rutas o Enlaces
const APP_ROUTES : Routes =
  [
      { path : 'home' , component : HomeComponent },
      { path : 'heroes' , component : HeroesComponent },
      { path : 'about' , component : SobreComponent },
      { path : 'heroe/:id' , component : HeroeComponent },
      { path : '**', pathMatch :'full', redirectTo : 'home' }
  ];

export const APP_ROUTING = RouterModule.forRoot( APP_ROUTES );
