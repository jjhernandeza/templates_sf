import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Usuario.RoutesComponent } from './usuario.routes.component';

describe('Usuario.RoutesComponent', () => {
  let component: Usuario.RoutesComponent;
  let fixture: ComponentFixture<Usuario.RoutesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Usuario.RoutesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Usuario.RoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
