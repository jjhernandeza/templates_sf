import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas',
  templateUrl: './directivas-personalizadas.component.html',
  styleUrls: ['./directivas-personalizadas.component.css']
})
export class DirectivasPersonalizadasComponent implements OnInit {

  constructor() { }

  color : string = 'orange';
  ngOnInit(): void {
  }

}
