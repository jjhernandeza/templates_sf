import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  templateUrl: '/css.component.html',
  styles: [ `
   p{
      font-size: 35px;
      color:red;
   }
  `
  ]
})
export class CssComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
