import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p style="font-size : 20px;"> Esta es una etiqueta HTML</p>
    <p [ngStyle]="{'font-size': tamanio+'px'}"> Esta es una etiqueta HTML</p>
    <p [style.fontSize.px]="45"> Esta es una etiqueta HTML</p>
    <p [style.fontSize.px]="tamanio"> Esta es una etiqueta HTML</p>

    <button class="btn btn-primary" (click)="tamanio = tamanio + 5"><i class="fa fa-plus"></i></button>
    <button class="btn btn-primary" (click)="tamanio = tamanio - 5"><i class="fa fa-minus"></i></button>
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {

  tamanio: number = 30;

  constructor() { }

  ngOnInit(): void {
  }

}
