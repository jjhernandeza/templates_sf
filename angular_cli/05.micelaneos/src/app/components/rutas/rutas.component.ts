import {

  Component
  ,OnInit
,OnChanges
,DoCheck
,AfterContentInit
,AfterContentChecked
,AfterViewInit
,AfterViewChecked
,OnDestroy


} from '@angular/core';

@Component({
  selector: 'app-rutas',
  /*templateUrl: './rutas.component.html',*/
  template : `
    <h3 style="color:darkred">Desde el componente Rutas y Rutas hijas</h3>
    <app-ng-style class="m-3"></app-ng-style>
    <app-css></app-css>
    <app-ng-class class="m-3"></app-ng-class>
  `,
  styleUrls: ['./rutas.component.css']
})
export class RutasComponent implements OnInit, OnChanges
,DoCheck
,AfterContentInit
,AfterContentChecked
,AfterViewInit
,AfterViewChecked
,OnDestroy {

  constructor() {
    console.log( "desde el primer ROETER");
  }

  ngOnInit(): void {
  }

  ngOnChanges()
  {
    console.log( "OnChanges" );
  }

  ngDoCheck()
  {
    console.log( "DoCheck" );
  }

  ngAfterContentInit()
  {
    console.log( "AfterContentInit" );
  }

  ngAfterContentChecked()
  {
    console.log( "AfterContentChecked" );
  }

  ngAfterViewInit()
  {
    console.log( "AfterViewInit" );
  }

  ngAfterViewChecked()
  {
    console.log( "AfterViewChecked" );
  }

  ngOnDestroy()
  {
    console.log( "OnDestroy" );
  }

}

