import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.css']
})
export class NgClassComponent implements OnInit {

  alerta      : string = 'alert alert-success d-flex align-items-center';
  tipoalart   : string = 'd-flex align-items-center';
  propiedades : any    =  { danger : true } ;

  constructor()
  {
  }

  ngOnInit(): void {
  }

  danger ()
  {
      this.propiedades.danger = true;
      console.log( this.propiedades.danger );
  }

}
