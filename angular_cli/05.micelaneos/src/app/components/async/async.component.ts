import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async',
  templateUrl: './async.component.html',
  styleUrls: ['./async.component.css']
})
export class AsyncComponent implements OnInit {

  working : boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  ejecutar ()
  {
    this.working = true;

    setTimeout( ()=>  this.working = false , 6000);
  }

}
