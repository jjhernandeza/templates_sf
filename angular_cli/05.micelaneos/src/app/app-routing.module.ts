/*import { NgModule } from '@angular/core';*/
import { RouterModule, Routes } from '@angular/router';
import { RutasComponent } from './components/rutas/rutas.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { USUARIO_ROUTES } from "./components/usuario/usuario.routes";

const APP_ROUTES: Routes = [
  { path: "Home", component : RutasComponent },
  {
      path       : "Usuario/:id"
      ,component : UsuarioComponent
      ,children  : USUARIO_ROUTES
  },
  { path: '**', pathMatch: 'full', redirectTo: 'Home' }
];

export const APP_ROUTING = RouterModule.forRoot( APP_ROUTES );

/*
@NgModule({
  imports: [ RouterModule.forRoot( APP_ROUTES ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }*/
