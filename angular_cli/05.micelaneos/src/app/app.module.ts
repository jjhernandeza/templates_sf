import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from "@angular/common/http";

//Main
import { AppComponent } from './app.component';

//Rutas
/*import { APP_ROUTING } from './app.routes';*/
import { APP_ROUTING } from './app-routing.module';


//Compomenentes
import { NgStyleComponent } from './components/ng-style/ng-style.component';
import { CssComponent } from './components/css/css.component';
import { NgClassComponent } from './components/ng-class/ng-class.component';
import { AsyncComponent } from './components/async/async.component';
import { NgSwitchComponent } from './components/ng-switch/ng-switch.component';
import { RutasComponent } from './components/rutas/rutas.component';


//Directivas
import { ResaltadoDirective } from './directives/resaltado/resaltado.directive';
import { DirectivasPersonalizadasComponent } from './components/directivas-personalizadas/directivas-personalizadas.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { UsuarioNuevoComponent } from './components/usuario/usuario-nuevo.component';
import { UsuarioEditarComponent } from './components/usuario/usuario-editar.component';
import { UsuarioDetalleComponent } from './components/usuario/usuario-detalle.component';
import { NavbarComponent } from './components/navbar/navbar.component';


//Pipes

@NgModule({
  declarations: [
    AppComponent,
    NgStyleComponent,
    CssComponent,
    NgClassComponent,
    AsyncComponent,
    DirectivasPersonalizadasComponent,
    ResaltadoDirective,
    NgSwitchComponent,
    RutasComponent,
    UsuarioComponent,
    UsuarioNuevoComponent,
    UsuarioEditarComponent,
    UsuarioDetalleComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    /*FormsModule,
    HttpClientModule,*/
    APP_ROUTING,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
