import { RouterModule,  Routes } from '@angular/router';
import { RutasComponent } from "./components/rutas/rutas.component";

const APP_ROUTES: Routes = [
  { path: "Home", component : RutasComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

export const APP_ROUTING = RouterModule.forRoot( APP_ROUTES );
