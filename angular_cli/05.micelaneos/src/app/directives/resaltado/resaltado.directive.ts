import { Directive, ElementRef, HostListener ,Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective
{
  constructor( private _elemRef:ElementRef )
  {
      console.log("Directiva llamada ");
      //_elemRef.nativeElement.style.backgroundColor="yellow";
  }

  /*se tiene que asignar un valor por defecto para la salida al momento de recibir el parametro
  el parámetro*/
  @Input("appResaltado") nuevoColor: string = '';

  @HostListener('mouseenter') mouseEntro = () => {
    this.reseltar( this.nuevoColor );
  };

  @HostListener('mouseleave') mouseSalida()
  {
    //this._elemRef.nativeElement.style.backgroundColor = null;
     this.reseltar( '' );
  }

  private reseltar( color : string  = 'yellow' )
  {
      this._elemRef.nativeElement.style.backgroundColor = color;
  }
}
