"use strict";
//Tipado de Retorna de una función
//import { Calculo } from 'calculo';
(() => {
    //Repuestas
    // 1)
    // Uso de Let y Const
    /*var nombre = 'Ricardo Tapia';
    var edad = 23;

    var PERSONAJE = {
        nombre: nombre,
        edad: edad
    };*/
    // Uso de Let y Const
    let nombre = 'Ricardo Tapia';
    let edad = 23;
    const PERSONAJE = {
        nombre: nombre,
        edad: edad
    };
    console.log(nombre);
    console.log(edad);
    console.log(PERSONAJE);
    const batman = {
        nombre: 'Bruno Díaz',
        tecnica: ['Karate', 'Aikido', 'Wing Chun', 'Jiu-Jitsu']
    };
    //3)
    const resultado_doble = (a, b) => { return (a + b) * 2; };
    //4)
    const getAvenger = (nombre, poder, arma = "Cuchillo relámpago") => {
        if (poder)
            console.log(` ${nombre} tiene el poder de ${poder} y un arma : ${arma}`);
        else
            console.log(` ${nombre} tiene un arma : ${arma}`);
    };
    class Calculo {
        constructor(a, b) {
            this.a = a;
            this.b = b;
            this.getArea = () => this.a * this.b;
        }
    }
})();
