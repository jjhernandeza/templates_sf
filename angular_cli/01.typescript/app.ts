//Tipado de Retorna de una función
//import { Calculo } from 'calculo';

( ()=>
    {

        //Repuestas
        // 1)
        // Uso de Let y Const
        /*var nombre = 'Ricardo Tapia';
        var edad = 23;

        var PERSONAJE = {
            nombre: nombre,
            edad: edad
        };*/

        // Uso de Let y Const
        let nombre = 'Ricardo Tapia';
        let edad   = 23;

        const PERSONAJE =
            {
                nombre : nombre
                ,edad  : edad
            };

        console.log( nombre );
        console.log( edad );
        console.log( PERSONAJE );


        //2)
        interface dcComic
        {
            nombre   ?: string
            ,tecnica ?: string[] //Array
        }

       const batman : dcComic =
        {
            nombre   : 'Bruno Díaz'
            ,tecnica : ['Karate','Aikido','Wing Chun','Jiu-Jitsu']
        }

        //3)
        const resultado_doble = ( a:number, b:number) => { return ( a + b ) * 2 };

        //4)
        const getAvenger =
            (
                nombre : string
                ,poder ?: string
                ,arma  : string = "Cuchillo relámpago"
            ) =>
            {
                if( poder )
                    console.log( ` ${ nombre } tiene el poder de ${ poder } y un arma : ${ arma }` );
                else
                    console.log( ` ${ nombre } tiene un arma : ${ arma }` );
            };


        class Calculo
        {

            constructor(  public a:number , public b:number )
            {
            }

            getArea = ():number => this.a * this.b;
        }

    }

)();