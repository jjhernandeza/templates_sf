//Interfaces

( ()=>
    {
        //Permite definir reglas para cumnpliar condiones de tipado de datos u objetos
        interface Xmen
        {
            nombre : string,
            edad   : number,
        }

        //Funciones sin Referencias hacia Interfaces
        const enviarMision = ( xmen : {  nombre: string } ) =>
        {
            console.log(`Enviado a ${ xmen.nombre } misión`);
        };

        const regrasarMision = ( xmen: { nombre: string } ) =>
        {
            console.log(`Regresando a ${ xmen.nombre } de la misión`);
        };

        //Funciones con Referencias hacia Interfaces
        const enviarMision_1 = ( xmen : Xmen ) =>
        {
            console.log(`Enviado a ${ xmen.nombre } misión`);
        };

        const regrasarMision_2 = ( xmen: Xmen ) =>
        {
            console.log(`Regresando a ${ xmen.nombre } de la misión`);
        };

        const x_men =
            {
                nombre : "Logan"
                ,edad  : 60
            };

        //Referencia hacia interfaz
        const x_men_2 : Xmen =
            {
                nombre : "Magneto"
                ,edad  : 30
            };

        enviarMision( x_men );
        regrasarMision( x_men );

        enviarMision_1( x_men_2 );
        regrasarMision_2( x_men_2 );
    }
)();