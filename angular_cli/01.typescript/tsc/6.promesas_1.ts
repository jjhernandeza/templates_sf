( ()=>
    {
        //Promesas
        console.log( "inicio");

        const promise_1 = new Promise(( resolve, reject ) =>
        {
            setTimeout(()=>{ resolve('Se resolvio la promesa')},1000);
            setTimeout(()=>{ reject('Se resolvio la promesa')},1000);
        });

        promise_1.then( ( mensaje )=> console.warn( mensaje ) ).catch( ( error ) => console.error( error ) );

        console.log( "Fin");
    }
)();