(function ()
{
    //Parámtros

    function activar
    (
        quien     : string //Argumento Obligatorio
        ,momento? : string // Argumento opcional
        ,objeto   : string = 'batiseñal' //Argumento Predefinido
    )
    {

        if( momento )
            console.log( `${ quien } activo la ${ objeto } en la ${ momento }` );
        else
            console.log(`${ quien } activo la ${ objeto }`);

    }

    activar('Gordon');
    activar('Gordon', 'tarde');
})();