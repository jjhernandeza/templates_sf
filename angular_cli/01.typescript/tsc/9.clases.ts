//Clases

( ()=>
    {
        //Inicialización de Variables al Constructror

        /*class Vengadores
        {
            1) Inicialición de Constructores
            nombre  : string  = "Steve";
            equipo    : string  = "Capi";
            nom_hereo : string  = "Capitan America";
            pelear    : boolean = true;
            victorias : number  = 15;

            2) Inicialición de Constructores ( Por argumentos, si se define en la llamdda )
            nombre  ?: string;
            equipo    ?: string;
            nom_hereo ?: string;
            pelear    ?: boolean;
            victorias ?: number;

            3) Parámetros sin definición
            nombre    : string;
            equipo    : string;
            nom_hereo : string;
            pelear    : boolean;
            victorias : number;

            constructor
            (
                nombre     : string
                ,equipo    : string
                ,nom_hereo : string
                ,pelear    : boolean
                ,victorias : number
            )
            {
                this.nombre = nombre;
                this.equipo = equipo;
                this.nom_hereo = nom_hereo;
                this.pelear = pelear;
                this.victorias = victorias;
            }
        }

        const antman = new Vengadores('Atman');

        console.log( antman );*/

        //Contruncciones Publicas ( public, private )
        class Vengadores
        {
            constructor
            (
                public nombre      ?: string
                ,public equipo     ?: string
                ,public nom_hereo  ?: string
                ,public pelear     ?: boolean
                ,public victorias  ?: number
            )
            {

            }
        }

        const antman = new Vengadores('Steve Roger', 'Capi');

        console.log( antman );
    }
)();