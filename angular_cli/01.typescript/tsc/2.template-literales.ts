(function ()
{

    function getEdad()
    {
        return 100+300+100;
    }

    const nombre   = 'Juan Jose',
          epellido = 'Hernandez',
          edad     = '30';

    const salida_1 = nombre +" "+edad+" ( edad: "+edad+ " )";
    const salida_2 = `${ nombre } ${ epellido } ( edad: ${ edad })`; //Template Literales
    const salida_3 = `${ nombre } ${ epellido } ( edad: ${ getEdad() })`; //Template Literales

    console.log( salida_1 );
    console.log( salida_2 );
    console.log( salida_3 );

})();