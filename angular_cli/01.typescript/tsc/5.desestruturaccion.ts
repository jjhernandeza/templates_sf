( ()=>
    {
        //Desestructuración de Objetos
        const avenger =
            {
                nombre : 'Steve'
                ,clave : 'Capitán América'
                ,poder : 'Droga'
            };

        //Permite crear variables apartir de objetos definidos
        const { nombre, clave, poder } = avenger;

        console.log( nombre );
        console.log( clave );
        console.log( poder );

        const extraer = ( avenger : any ) =>
        {
            const { nombre, poder } =  avenger;

            console.log( nombre );
            console.log( clave );
            console.log( poder );

        };

        //Especificación el tipo de artigumentos que traer el objeto
        const superH  = ( {nombre, poder } : any ) =>
        {
            console.log( nombre );
            console.log( poder );
        };

        extraer( avenger );
        superH( avenger );

        const vengadores:string[] = ['Thor','Iroman','Spider'];//Se indica que el arreglo será tipo string

        //Se indica el tipo de de desestructuración de objetos {} o arreglos []

        const [ asdgar , tecno, natural ] = vengadores;//Se define el nombre de la variable según la posición del elemento
        const [  , , arania ] = vengadores;//En caso de solicitar la posición deseada

        console.log( asdgar );
        console.log( tecno );
        console.log( natural );


        const superheroes = { 'marvel':['Thor','Iroman','Spider'], 'dcComic':['Batman','Acuaman','Flash'] };

        const {  marvel, dcComic  } = superheroes;

        console.log( marvel );
        console.log( dcComic );

        const [ uno, dos , tres ] = marvel;
        const [ cuatro, cinco, seis ] = dcComic;

        console.log( uno );
        console.log( dos );
        console.log( tres );

        console.log( cuatro );
        console.log( cinco );
        console.log( seis );
    }
)();