( ()=>
    {
        const retirarDinero = ( monto: number) =>
        {
            let saldo = 1000;

            return new Promise((resolve, reject) =>
            {

                if (saldo < monto)
                    return reject('No hay fondos suficientes');

                saldo -= monto;
                return resolve(saldo);

            });
        };

        //Definición de tipado (Number, String, Objeto, etc)
        const retiroEfectivo = ( monto: number) : Promise<number>=>
        {
            let saldo = 1000;

            return new Promise((resolve, reject) =>
            {

                if (saldo < monto)
                    return reject('No hay fondos suficientes');

                saldo -= monto;
                return resolve(saldo);

            });
        };

        retirarDinero( 500 )
            .then( (montoActual) => console.info( `El saldo actual ${ montoActual }` ) )
            .catch( ( error ) => console.error( error )  );

        retirarDinero( 1500 )
            .then( (montoActual) => console.info( `El saldo actual ${ montoActual }` ) )
            .catch( ( error ) => console.error( error )  );

        //Expresión simple de Respuesta
        retirarDinero( 854.5 )
            .then( (montoActual) => console.info( `El saldo actual ${ montoActual }` ) )
            .catch( console.error );
    }
)();