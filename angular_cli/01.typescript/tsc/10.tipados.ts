//Tipado de Retorna de una función

( ()=>
    {
        const sumar   = ( a:number, b:number ): number => {  return a +b; };
        const sumar_1 = ( a:number, b:number ): number => a + b;

        console.log( sumar( 5 , 9) );

        const nombre = ():string => "Hola mundo";

        //Expresiones definidas del tipo de resoluciones de Respuesta, return, promise, efecto ( Tipado de funciones )
       /* const obtener_salario = ():Promise <string> =>
        {
            return new Promise( ( resolve, reject ) =>
            {
                resolve( 'Tipo de Funciones');
            });
        };*/

       // obtener_salario().then( a => console.log( a.toUpperCase() ) );
    }
)();