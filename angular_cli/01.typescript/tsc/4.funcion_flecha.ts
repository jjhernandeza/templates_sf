(function ()
{

    //Funciones de Flecha
   let mifuncion = function ( a:string )
   {
       return a.toUpperCase();
   };

   let mifuncionF  = ( a:string ) => { return a.toUpperCase(); }

    console.log( mifuncion( "funcion normal") );
    console.log( mifuncionF("funcion flecha") );

    const sumar = function ( a:number , b:number )
    {
        return a + b;
    };

    const suma = ( a:number, b:number ) => { return a + b; };

    console.log( sumar( 15 , 87) );
    console.log( sumar( 155 , 78) );

    const hulk =
        {
            nombre : "Hulk"
            ,smash()
            {
                setTimeout( () => {
                    console.log( `${this.nombre} es muy fuerte`  );
                },1000);
            }

        };

    hulk.smash();
})();