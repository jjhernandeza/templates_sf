
//Decoradores, expande la clase añadiendo características y funcionalidades propias a una clase

function  consutrctorClase ( parametro : Function )
{
    console.warn( parametro );
}

@consutrctorClase
export class Xmen
{
    constructor
    (
        public  nombre ?: string,
        public  clave  ?: string
    ){}

    //Define funciones o variables de uso para la clase
    imprimir()
    {
        console.warn( `${ this.nombre } - ${ this.clave }`);
    };

}