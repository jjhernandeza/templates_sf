
//Permitir si la clase Será utlizada en otras librerias o dependecias a otros clases, de no requerirlo se refiere a una clase privada
export class Xmen
{
    constructor
    (
        public  nombre ?: string,
        public  clave  ?: string
    ){}

    //Define funciones o variables de uso para la clase
    imprimir()
    {
        console.warn( `${ this.nombre } - ${ this.clave }`);
    };

}