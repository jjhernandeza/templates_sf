import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/login/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private _authService : AuthService, private _router: Router ) {
  }

  //Autenticación para activar a las rutas a la que se desea navegar
  canActivate( ) : boolean
  {
      if( !this._authService.autenticacionActiva() )
        this._router.navigateByUrl('/login');
      else
        return true;
  }

}
