import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { UsuarioModel } from "../../models/usuario/usuario.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService
{
    private URL       :string = "https://identitytoolkit.googleapis.com/v1/accounts:";
    private APIKEY    :string = "AIzaSyDctbTkmz3ZkSlQ5SnYk70y2ORndm8v6DU";
    private userToken :string;

    //Crear nuevo usuarios
    //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]
    //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

    //Acceso por usuario
    //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]
    constructor( private _serviceHtpp : HttpClient )
    {
      this.leerToken();
    }

    iniciar_sesion( usuario : any )
    {
        //En su caso de que el objeto de petición sea igual al obejeto esperado en lado del BACKEND se puede utilizar la siguiente expresion.
        /*const authData =
          {
             ...usuario
          };*/

        const authData =
        {
            email: usuario.EMAIL
            ,password: usuario.PASSWORD
            ,returnSecureToken: true
        };

        //Petición de tipo Htpp : POST ( this.nombre_instancia.post( endpoint, parametros_consulta ) );
        //return this._serviceHtpp.post(`${ this.URL }signInWithPassword?key=${ this.APIKEY }`,authData );
        return this._serviceHtpp.post(`${ this.URL }signInWithPassword?key=${ this.APIKEY }`,authData )
          .pipe( map( resp => {
            this.guardarToken( resp['idToken'] );
            return resp;
          } ));
    }

    nuevo_usuario ( usuario : UsuarioModel )
    {
        //En su caso de que el objeto de petición sea igual al obejeto esperado en lado del BACKEND se puede utilizar la siguiente expresion.
        /*const authData =
          {
             ...usuario
          };*/

        //Donde la expresión de que incluya todo el arreglo del objeto en el nuevo objeto de petición o solicitud

        const authData =
        {
            email: usuario.email
            ,password: usuario.password
            ,returnSecureToken: true
        };

        //Petición de tipo Htpp : POST ( this.nombre_instancia.post( endpoint, parametros_consulta ) );
        //return this._serviceHtpp.post(`${ this.URL }signUp?key=${ this.APIKEY }`,authData );
        return this._serviceHtpp.post(`${ this.URL }signUp?key=${ this.APIKEY }`,authData )
          .pipe( map( resp => {
            this.guardarToken( resp['idToken'] );
            return resp;
          } ));
    }

    cerrar_session ( )
    {
        localStorage.removeItem('token');
    }

    private guardarToken ( idToken:string )
    {
        this.userToken = idToken;
        localStorage.setItem('token', idToken);

        let hoy = new Date();

        hoy.setSeconds( 3600 );

        localStorage.setItem('expira', hoy.getTime().toString() )
    }

    leerToken ()
    {
        if( localStorage.getItem('token') )
            this.userToken = localStorage.getItem('token');
        else
          this.userToken = '';
    }

    autenticacionActiva () : boolean
    {
        if( this.userToken.length < 2 )
            return  false;

        const vigencia = Number( localStorage.getItem('expira') );
        const expira  = new Date();
        expira.setTime( vigencia );

        return expira > new Date();
    }

}
