import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/login/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _authService : AuthService,  private _router : Router ) { }

  ngOnInit() {
  }

  cerrar_sesion()
  {
      this._authService.cerrar_session();
      this._router.navigateByUrl('/login');
  }

}
