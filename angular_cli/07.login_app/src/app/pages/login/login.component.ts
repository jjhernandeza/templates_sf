import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/login/auth.service";
import Swal from 'sweetalert2';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario : any =
    {
        EMAIL     : null
        ,PASSWORD : null
    };

  recordar = false;

  constructor( private _authService : AuthService, private _router: Router ) { }

  ngOnInit()
  {
        if( localStorage.getItem('email') )
        {
          this.usuario.EMAIL = localStorage.getItem('email');
          this.recordar = true;
        }
  }

  login( frm_login : NgForm )
  {
      if( frm_login.form.status  !== 'INVALID' )
      {

          Swal.fire({
            allowOutsideClick: false,
            icon: 'info',
            text: 'Espera por Favor..'});
          Swal.showLoading();

          this._authService.iniciar_sesion( this.usuario ).subscribe( ( data ) =>
          {
            console.log(data);
            Swal.close();

            if( this.recordar )
              localStorage.setItem('email' , this.usuario.EMAIL );

            this._router.navigateByUrl('/home');

          },  ( err ) =>
          {
            Swal.fire({
              icon: 'error',
              title:'Error al autenticar',
              text:  err.error.error.message });
          });
      }
      else
      {
        return;
      }
  }
}
