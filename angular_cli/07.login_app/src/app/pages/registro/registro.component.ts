import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";//Módulo para hacer uso del ngModel
import {Router} from "@angular/router";
import { UsuarioModel } from '../../models/usuario/usuario.model';
import {AuthService} from "../../services/login/auth.service";
import Swal from 'sweetalert2';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  /*
    ##Definir el modelo de datos en el formulario por un models.ts ó declararlo de tipo any
  *   usuario : any = {};

       this.usuario =
      {
          EMAIL        : null
          ,SEG_USUARIO : null
          ,PASSWORD    : null
      }
  * */
  usuario : UsuarioModel;
  recordar = false;
  constructor( private _authService: AuthService, private _router : Router ) { }

  ngOnInit()
  {
      // Parámetros
      this.usuario              = new UsuarioModel();
       this.usuario.email       = null;
       this.usuario.seg_usuario = null;
       this.usuario.password    = null;
  }

  registrarUsuario( form_registrar : NgForm )
  {
      if( form_registrar.form.status  === 'INVALID' )
          return;

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espera por Favor..'});
    Swal.showLoading();

      this._authService.nuevo_usuario( this.usuario ).subscribe( ( data ) =>
      {
          console.log(data);
          Swal.close();

        if( this.recordar )
          localStorage.setItem('email' , this.usuario.email );

          this._router.navigateByUrl('/home');
      },  ( err ) =>
      {

        Swal.fire({
          icon: 'error',
          title:'Error al autenticar',
          text:  err.error.error.message });
      });
  }

}
