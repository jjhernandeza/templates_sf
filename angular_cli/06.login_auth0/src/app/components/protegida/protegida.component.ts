import { Component, OnInit } from '@angular/core';
import {AuthService} from "@auth0/auth0-angular";

@Component({
  selector: 'app-protegida',
  templateUrl: './protegida.component.html',
  styleUrls: ['./protegida.component.css']
})
export class ProtegidaComponent implements OnInit {

  constructor( public authService: AuthService ) { }

  ngOnInit(): void
  {
    console.log( "init de la view protegida ")
      this.authService.user$.subscribe( perfil => { console.log( perfil ) } );
  }

}
