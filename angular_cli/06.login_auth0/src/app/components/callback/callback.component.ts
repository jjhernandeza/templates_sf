import { Component, OnInit } from '@angular/core';
import {AuthService2} from "../../services/auth/auth.service";

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor( private _authService : AuthService2) { }

  ngOnInit(): void
  {
    /*this._authService.handleAuthCallback();*/
  }

}
