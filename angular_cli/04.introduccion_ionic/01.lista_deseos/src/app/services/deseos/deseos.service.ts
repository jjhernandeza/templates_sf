
  import { Injectable } from '@angular/core';
  import { ListaModule } from "../../models/lista-item/lista.module";

  @Injectable({
    providedIn: 'root'
  })
  export class DeseosService
  {
      lista :ListaModule[] = [];

      constructor()
      {
          this.cargarStorage();
          console.log( this.lista );
      }

      obtener_lista ()
      {
          return this.lista;
      }

      crear_lista ( titulo :string )
      {
          const nuevalista = new ListaModule( titulo );

          console.log( nuevalista );

         /* this.lista.push( nuevalista );
          this.guardarStorege();

          return nuevalista.ID_LISTA;*/
      }

      obtener_detalle ( id_lista : string | number )
      {
          id_lista = Number( id_lista );
          return this.lista.find( listaData => listaData.ID_LISTA === id_lista );
      }

      guardarStorege ()
      {
          localStorage.setItem( "lista" , JSON.stringify( this.lista ) );
      }

      cargarStorage ()
      {
          this.lista = ( localStorage.getItem( "lista" ) === null || localStorage.getItem( "lista" ) === undefined ) ? this.lista : JSON.parse( localStorage.getItem( "lista" ) );
      }

      eliminarItemStorage ()
      {
          localStorage.removeItem( "lista" );
      }


  }
