  import { NgModule } from '@angular/core';
  import { CommonModule } from '@angular/common';



  @NgModule({
    declarations: [],
    imports: [
      CommonModule
    ]
  })
  export class ListaItemModule
  {
      DESCRIPCION : string;
      COMPLETADO  : boolean;

      constructor( desc: string )
      {
          this.DESCRIPCION = desc;
          this.COMPLETADO  = false
      }
  }
