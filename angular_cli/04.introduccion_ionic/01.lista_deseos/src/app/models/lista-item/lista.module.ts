  import { NgModule } from '@angular/core';
  import { CommonModule } from '@angular/common';
  import { ListaItemModule } from './lista-item.module';

  @NgModule({
    declarations: [],
    imports: [
      CommonModule
    ]
  })
  export class ListaModule
  {
      ID_LISTA    : number;
      TITULO      : string;
      CREADAEN    : Date;
      TERMINADAEN : Date;
      TERMINADA   : boolean;
      ITEMS       : ListaItemModule[];

      constructor( titulo:string )
      {
          this.TITULO    = titulo
          this.CREADAEN  = new Date();
          this.TERMINADA = false;
          this.ITEMS     = [];
          this.ID_LISTA  = new Date().getTime();
      }
  }
