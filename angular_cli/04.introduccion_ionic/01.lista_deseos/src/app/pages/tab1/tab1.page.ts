  import { Component } from '@angular/core';
  import { DeseosService } from "../../services/deseos/deseos.service";
  import { Router } from "@angular/router";
  import { AlertController } from "@ionic/angular";

  @Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
  })
  export class Tab1Page
  {
      listaDeseos : object = {};
      constructor( public _deseosService : DeseosService, private _router: Router, private _alertController : AlertController)
      {
           this.listaDeseos = _deseosService.obtener_lista();
      }

      //async transforma un método, una función en un promesa
      async agregarlista()
      {
          const alert = await this._alertController .create(
              {
                header    : "Nueva lista",
                inputs    : [{  name:'titulo', type:'text', placeholder :'Nombre de la lista' }],
                buttons   : [
                              {
                                text: 'Cancelar', role: 'cancel', handler: () => console.log('Cancelar')
                              },
                              {
                                  text: 'Crear'
                                  ,role: 'success'
                                  ,handler: (data) =>
                                  {
                                      if( data.length  === 0 )
                                        return;

                                      this._deseosService.crear_lista( data.TITULO );
                                  }

                              }
                            ],

              }
          );

          alert.present();

          //this._router.navigateByUrl('tabs/tab1/agregar').then( r => console.log() );
      }

  }
;
