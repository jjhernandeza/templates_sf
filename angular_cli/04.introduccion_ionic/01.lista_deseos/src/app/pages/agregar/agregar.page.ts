  import { Component, OnInit } from '@angular/core';
  import { DeseosService } from "../../services/deseos/deseos.service";
  import {ActivatedRoute} from "@angular/router";
  import { ListaModule } from "../../models/lista-item/lista.module";
  import {ListaItemModule} from "../../models/lista-item/lista-item.module";
  import {Tab1Page} from "../tab1/tab1.page";

  @Component({
    selector: 'app-agregar',
    templateUrl: './agregar.page.html',
    styleUrls: ['./agregar.page.scss'],
  })
  export class AgregarPage implements OnInit {

      lista  : ListaModule;
      nombreItem : string = '';
      constructor( private _deseosService : DeseosService, private _activateRoute : ActivatedRoute, private _tab1Page : Tab1Page )
      {
          const listaId = Number ( this._activateRoute.snapshot.paramMap.get('idLista') );
          this.lista = _deseosService.obtener_detalle( listaId );
      }

      ngOnInit() {
      }

      agregarItem ()
      {
          if( this.nombreItem.length === 0 )
            return;

          const newItem = new ListaItemModule( this.nombreItem );
          this.lista.ITEMS.push( newItem );

          this.nombreItem = null;
          this._deseosService.guardarStorege();
      }

      cambioCheck( item: ListaItemModule )
      {
          this._deseosService.guardarStorege();
          const pendientes = this.lista.ITEMS.filter( itemData => !itemData.COMPLETADO ).length;


      }

  }
