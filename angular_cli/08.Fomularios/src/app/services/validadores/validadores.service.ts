import { Injectable } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";

interface ErrorValidate
{
   [s:string] : boolean
}

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService
{

    constructor() { }

    //Validación Personalizadas
    noHerrera( control: FormControl ) : { noHerrera: boolean }
    {
        if( control.value?.toLowerCase() === 'herrera' )
        {
          return {
            noHerrera: true
          }
        }

        // @ts-ignore
      return null;
    }

    //Validaciones Personalidas
    validarPassword( pass_1: string, pass_2 : string )
    {
        return ( formGroup : FormGroup ) =>
        {
            const pass_ctrl_1 = formGroup.controls[ pass_1 ];
            const pass_ctrl_2 = formGroup.controls[ pass_2 ];

            if( pass_ctrl_1.value === pass_ctrl_2.value )
                pass_ctrl_2.setErrors( null );
            else
                pass_ctrl_2.setErrors( { noEsIgual : true } );
        }
    }

    //Validación Asisncrona
    existeUsuario( control: FormControl  ): Promise<ErrorValidate> | Observable<ErrorValidate>
    {

        if( !control.value )
        {
            // @ts-ignore
            return Promise.resolve( null );
        }

        return  new Promise( (resolve, rejects ) => {
            setTimeout( () =>
            {
                if( control.value === 'jjhernandeza' )
                    resolve({ existe : true } )
                else
                    { // @ts-ignore
                      resolve( null );
                    }
            }, 3500);
        });
    }
}
