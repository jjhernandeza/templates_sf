import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  constructor( private _httpClient : HttpClient) { }

  cat_paises ()
  {
      return this._httpClient.get('https://restcountries.com/v3.1/lang/spa')
          .pipe//Operador para dar formato al objeto de respuesta
          (
              map( ( resp : any ) =>
              {
                  return resp.map( ( pais : any ) =>
                  {
                      return { nombre : pais.name.common, codigo : pais.cca3 };
                  })
              })
          );
  }
}
