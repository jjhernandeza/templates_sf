import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validator, Validators, FormArray } from "@angular/forms";
import { ValidadoresService } from "../../services/validadores/validadores.service";

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit
{
      //frm_reactive : FormGroup;

      //Se puede inicialiar desde la variable o al constructor
      /*frm_reactive = new FormGroup(
      {
        NOMBRE    : new FormControl('' )
        ,APELLIDO : new FormControl('' )
        ,CORREO   : new FormControl('' )
      });*/
    frm_reactive     = new FormGroup({});
    pattern : string = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";

    constructor( private _formBuilder: FormBuilder, private _validadoresService : ValidadoresService )
    {
        //Se inicializa el Formulario Reactivo
        this.crearFormulario();
        this.cargarListener();
    }

    ngOnInit(): void {
    }

    //Expresiones para obtener validaciones de Formulario por una aproximación
    get pasatiempos()
    {
        return this.frm_reactive.get('PASATIEMPOS') as FormArray;
    }

    get nombreValido ()
    {
        return ( this.frm_reactive.get('NOMBRE')?.invalid && this.frm_reactive.get('NOMBRE')?.touched )
    }

    get appValido ()
    {
      return ( this.frm_reactive.get('APELLIDO')?.invalid && this.frm_reactive.get('APELLIDO')?.touched )
    }

    get emailValido ()
    {
      return ( this.frm_reactive.get('CORREO')?.invalid && this.frm_reactive.get('CORREO')?.touched )
    }

    get distritoValido()
    {
        return ( this.frm_reactive.get('DIRECCION.DISTRITO')?.invalid && this.frm_reactive.get('DIRECCION.DISTRITO')?.touched )
    }

    get ciudadValido()
    {
       return ( this.frm_reactive.get('DIRECCION.CIUDAD')?.invalid && this.frm_reactive.get('DIRECCION.CIUDAD')?.touched )
    }
    //Termina Expresiones para obtener validaciones de Formulario por una aproximación

    get passwordValido()
    {
        return ( this.frm_reactive.get('PASSWORD_1')?.invalid && this.frm_reactive.get('PASSWORD_1')?.touched )
    }

    get confirmPassword()
    {
        /*let bandera = false;

        if( this.frm_reactive.get('PASSWORD_2')?.invalid && this.frm_reactive.get('PASSWORD_2')?.touched )
           bandera = true;

       const pass_1 = this.frm_reactive.get('PASSWORD_1')?.value;
       const pass_2 = this.frm_reactive.get('PASSWORD_2')?.value;

       if( pass_1.toLowerCase() !== pass_2.toLowerCase() )
         bandera = true; this.invalid_pass = true;

        return bandera;*/

      return ( this.frm_reactive.get('PASSWORD_2')?.invalid && this.frm_reactive.get('PASSWORD_2')?.touched )

    }

    get usuarioValido()
    {
        return ( this.frm_reactive.get('SEG_USUARIO')?.invalid && this.frm_reactive.get('SEG_USUARIO')?.touched )
    }

    crearFormulario()
    {
        //Se crea la instancia para el tipo del formulario de tipo Reactivo
        //Creando un arreglo con la estructura de validación donde se inicia bajo un arreglo asociado al objeto. ( n numero de objetos ligados a un formulario
        //Estructra es la siguiente: nombredelobjeto : [ valor (vacio,null,undefined,bolean,number,string, etc.), validaciones sincronas o arrelgos de validaciones, validaciones asincronas o erreglos de validaciones ]

        this.frm_reactive = this._formBuilder.group(
          {
              NOMBRE       : new FormControl('', [ Validators.required, Validators.minLength( 5 ) ] )
              ,APELLIDO    : new FormControl('', [ Validators.required, this._validadoresService.noHerrera  ] )
              ,CORREO      : new FormControl('', [ Validators.required,Validators.pattern( this.pattern ) ] )
              ,SEG_USUARIO : ['', , this._validadoresService.existeUsuario ] //Validaciones asincronas ( Resuelven una promesa o un observable )
              ,PASSWORD_1  : new FormControl('', [ Validators.required ] )
              ,PASSWORD_2  : new FormControl('', [ Validators.required ] )
              //Crea un objeto de FormGroup:
              ,DIRECCION : this._formBuilder.group( {
                DISTRITO : new FormControl( '', [ Validators.required ] )
                ,CIUDAD  : new FormControl( '', [ Validators.required ] )
              })
              ,PASATIEMPOS : this._formBuilder.array([ ])
          },
          {
            validators  : this._validadoresService.validarPassword('PASSWORD_1','PASSWORD_2')
          });
    }

    agregarPasatiempo()
    {
        this.pasatiempos.push( this._formBuilder.control('', Validators.required ) );
    }

    borrarPasatiempo( i: number )
    {
      this.pasatiempos.removeAt( i );
    }

    guardar()
    {

        if( this.frm_reactive.status === "INVALID" )
        {
            Object.values( this.frm_reactive.controls ).forEach( control =>  control.markAllAsTouched() );
            return;
        }

        console.log( this.frm_reactive );

    }

  asignarValor()
  {
      //Para la asignación de Datos a los Modelo de datos es necesario crear la misma estructura del objeto asociado al nuevo objeto
     /* this.frm_reactive.setValue(
        {
          NOMBRE: "Juan José",
          APELLIDO: "Hernández Antonio",
          CORREO: "juanhernandeza@tabasco.gob.mx",
          DIRECCION: {
            DISTRITO: "Reforma",
            CIUDAD: "Villahermosa"
          },
          PASATIEMPOS : this._formBuilder.array([ ])
        }
      );*/

    //O en su caso reiniciar el objeto y agregar la estructura   del objeto asociado al nuevo objeto
     this.frm_reactive.reset(
       {
         NOMBRE: "Juan José",
         APELLIDO: "Hernández Antonio",
         CORREO: "juanhernandeza@tabasco.gob.mx",
         SEG_USUARIO : 'jjhernandeza',
         PASSWORD_1 : "temporal",
         PASSWORD_2 : "temporal",
         DIRECCION: {
           DISTRITO: "Reforma",
           CIUDAD: "Villahermosa"
         }
       }
     );

      const data = [ 'Programar','Trabajar','Limpiar'];

      data.forEach(  ( value , item ) =>  this.pasatiempos.push( this._formBuilder.control(value ) ) );

      /*this.pasatiempos.push( this._formBuilder.control('Programar', Validators.required ) );
      this.pasatiempos.push( this._formBuilder.control('Trabajar', Validators.required ) );
      this.pasatiempos.push( this._formBuilder.control('Limpiar', Validators.required ) );*/
  }

  limpiarFormulario()
  {
      this.frm_reactive.reset(
          {
              NOMBRE      : "",
              APELLIDO    : "",
              CORREO      : "",
              SEG_USUARIO : "",
              PASSWORD_1  : "",
              PASSWORD_2  : "",
              DIRECCION   : { DISTRITO: "", CIUDAD: "" },
              PASATIEMPOS : this._formBuilder.array([ ])
          }
      );
  }

  cargarListener()
  {
      //Observables, Listeners o Watcher ( en su versión para AngularJS )
      this.frm_reactive.valueChanges.subscribe( valor => console.log( valor ) );

      this.frm_reactive.statusChanges.subscribe( valor => console.log( valor ) );

      this.frm_reactive.get('NOMBRE')?.valueChanges.subscribe( console.log );
  }


}
