import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import {PaisesService} from "../../services/paises/paises.service";

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit
{

  parametros : any =
  {
      NOMBRE     : null
      ,APELLIDOS : null
      ,EMAIL     : null
      ,PAIS      : ''
      ,GENERO    : null
  };

  validacion : any =
    {
        EMAIL_PATTERN : "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
       ,FRM_VALIDATE  : true
    };

  catpaises : any[] = [];
  constructor( private _paisService : PaisesService ) { }

  ngOnInit(): void
  {
      this.cat_paises();
  }

  guardar ( frmTemplate : NgForm )
  {
      if( frmTemplate.form.status === "INVALID" )
      {
          Object.values( frmTemplate.form.controls ).forEach( control =>  control.markAllAsTouched() );
          return;
      }

      console.log(frmTemplate.form);
      console.log(frmTemplate.form.value );
  }

  cat_paises ()
  {
      this._paisService.cat_paises()
          .subscribe( paises =>
          {
              this.catpaises = paises;

              //Agregar una posición vacía en caso de tener ningún valor por defecto
              this.catpaises.unshift({
                  nombre : '[- Seleccione una opción -]',
                  codigo : ''
              } );

              this.validacion.FRM_VALIDATE = false;
          });
  }

}
